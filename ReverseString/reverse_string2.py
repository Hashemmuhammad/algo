"""
given a string, return a new string with the reversed order of characters

---Examples
reverse('hashem') == 'mehsah'
reverse('hello') == 'olleh'

"""

import functools


def reverse(string: str) -> str:
    return functools.reduce(lambda reversed_str, char : char + reversed_str, string)


# result = reverse('hashem')
# print(result)