"""
given a string, return a new string with the reversed order of characters

---Examples
reverse('hashem') == 'mehsah'
reverse('hello') == 'olleh'

"""

def reverse(string: str) -> str:

    reversed_str : str = ''
    for char in string:
        reversed_str = char + reversed_str 
    return reversed_str


# result = reverse('hashem')
# print(result)