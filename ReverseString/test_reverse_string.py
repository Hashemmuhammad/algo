import unittest
from reverse_string import reverse


class TestReverse(unittest.TestCase):

    def test_reverse(self):
        
        self.assertEqual(reverse('hello'), 'olleh', 'sholud be olleh')
        


if __name__ == '__main__':
    unittest.main()